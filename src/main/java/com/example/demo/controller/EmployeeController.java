package com.example.demo.controller;

import com.example.demo.model.Employee;
import com.example.demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v2/")
public class EmployeeController {

    private EmployeeService employeeService;

    @Autowired
    public void setEmployeeService(EmployeeService employeeService){
        this.employeeService = employeeService;
    }
    @GetMapping("")
    public ResponseEntity<Map<String, Object>> findAll(){
        Map<String, Object> map = new HashMap<>();
        map.put("message","record found");
        map.put("data", employeeService.findAll());
        map.put("statue" , HttpStatus.OK);
        return  new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<Map<String, Object>> Addemp(@RequestBody Employee employee){
        Map<String, Object> map = new HashMap<>();
        map.put("message","Sucess");
        employeeService.AddEmp(employee);
        map.put("statue" , HttpStatus.CREATED);
        return  new ResponseEntity<Map<String, Object>>(map,HttpStatus.CREATED);
    }

    @RequestMapping("{id}")
    public  ResponseEntity<Map<String, Object>> findbyid(@PathVariable  int id){
        Map<String, Object> map = new HashMap<>();
        map.put("message","record found");
        map.put("data", employeeService.findById(id));
        map.put("statue" , HttpStatus.OK);
        return  new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<Map<String, Object>> updateemp(@PathVariable int id, @RequestParam String fname, String lname, String email ){
        Map<String, Object> map = new HashMap<>();
        map.put("Message" , "Record updated.");
        employeeService.UpdateEmp(id, fname, lname, email);
        map.put("statue" , HttpStatus.OK);
        return  new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);


    }

    @DeleteMapping("{id}")
    public ResponseEntity<Map<String, Object>> delete(@PathVariable  int id){
        Map<String, Object> map = new HashMap<>();
        map.put("message","record delete");
        employeeService.DeleteEmp(id);
        map.put("statue" , HttpStatus.OK);
        return  new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);
    }

}

package com.example.demo.repository;

import com.example.demo.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class EmployeeRepository {

    public List<Employee> employees = new ArrayList<>();
    {
        employees.add(new Employee(1,"Chea","viseth","viseth@gmail.com"));
        employees.add(new Employee(2,"Chea1","viseth1","viseth1@gmail.com"));
        employees.add(new Employee(3,"Chea2","viseth2","viseth2@gmail.com"));
    }

    public List<Employee> findAll(){
        return employees;
    }

    public Employee findbyId(int id){
        Employee employee = new Employee();
        for (Employee emp: employees
             ) {
            if(emp.getId()==id) {
                employee = emp;
            }
        }
        return employee;
    }

    public void AddEmployee(Employee employee){
        employees.add(employee);
    }

    public void UpdateEmployee(int id ,String fname , String lname , String email){
        Employee employee = this.findbyId(id);
        employee.setFirstname(fname);
        employee.setLastname(lname);
        employee.setEmail(email);
    }

    public void DeleteEmployee(int id){
        Employee employee = this.findbyId(id);
        employees.remove(employee);
    }

}

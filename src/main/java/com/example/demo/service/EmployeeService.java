package com.example.demo.service;

import com.example.demo.model.Employee;

import java.util.List;

public interface EmployeeService {

    List<Employee> findAll();
    Employee findById(int id);
    void AddEmp(Employee employee);
    void UpdateEmp(int id , String fname , String lname , String email);
    void DeleteEmp(int id);
}

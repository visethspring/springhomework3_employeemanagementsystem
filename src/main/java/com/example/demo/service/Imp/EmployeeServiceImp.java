package com.example.demo.service.Imp;

import com.example.demo.model.Employee;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImp implements EmployeeService {

    private EmployeeRepository employeeRepository;

    @Autowired
    public void setEmployeeRepository(EmployeeRepository employeeRepository){
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee findById(int id) {
        return employeeRepository.findbyId(id);
    }

    @Override
    public void AddEmp(Employee employee) {
        employeeRepository.AddEmployee(employee);
    }

    @Override
    public void UpdateEmp(int id, String fname , String lname , String email) {
        employeeRepository.UpdateEmployee(id, fname, lname, email);
    }


    @Override
    public void DeleteEmp(int id) {
        if(id < 1){
            id = 1;
        }
        else employeeRepository.DeleteEmployee(id);
    }
}
